from flask import Flask, render_template, request, jsonify
import dns
import dns.resolver as dnsResolve
import re
import smtplib

app = Flask(__name__)

@app.route("/")
def index():
    print("Welcome")
    return "Welcome"

def verifyFormat(email_address):
    # source: https://stackoverflow.com/a/35094475
    match = re.match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", email_address)
    if match==None:
        # Not in format? return False
        return False
    else:
        # Follow standard email format? return True
        return True

def obtainDomain(email_address):
    try:
        match = email_address.split('@')[1]
        return match
    except IndexError as ee:
        print("Not a valid email address")
        return False
    except Exception as e:
        # print(e.__name__)
        print("Something error: "+str(e))
        return False

def obtainMXRecord(domain_name):
    val_dict_ = {}
    sum_dict_ = {}
    boole_ = False
    try:
        r = dnsResolve.resolve(domain_name, "mx")
        for val in r:
            parse_val = val.to_text().split(" ")
            parsedVal_priority = parse_val[0]
            parsedVal_data = parse_val[1]
            val_dict_[parsedVal_priority] = parsedVal_data
            sum_dict_["mx_record"] = "Found"
        # return True
        boole_ = True
    except dnsResolve.NoAnswer as err_NoA:
        print("Mail Exchange (MX) record does not found for this domain name")
        # return False
        boole_ = False
    except Exception as e:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)
        # return False
        boole_ = False
    # if val_dict_:
    #     getHighestPriority = min(val_dict_, key=int)
    #     getHighestPriority = val_dict_[getHighestPriority]
    #     return getHighestPriority
    # else:
    #     print("ok")
    #     return None
    if boole_:
        getHighestPriority = min(val_dict_, key=int)
        getHighestPriority = val_dict_[getHighestPriority]
        return getHighestPriority
    else:
        return None

def checkSMTP(email_address, mx_record):
    print("Now we try to connect to this SMTP")
    server = smtplib.SMTP(host=mx_record, timeout=4)
    server.connect(mx_record, 25)
    server.ehlo()
    server.mail("verifier@gmail.com")
    code, message = server.rcpt(str(email_address))
    # print(server.verify(email_address))
    if code == 250:
        # print("success. email found")
        return True
    else:
        # print("email not found")
        return False

def main_program(email_address):
    print("Checking email: "+str(email_address)+" ...")
    if verifyFormat(email_address):
        print("Email format: Valid")
        print("Get the domain from email address...")
        if obtainDomain(email_address):
            print("Domain obtained: "+str(obtainDomain(email_address)))
            print("Check MX record for this domain")
            queryDomain_ = obtainDomain(email_address)
            # print("check mx record data: " +str(type(obtainMXRecord(queryDomain_))))
            # v = lambda x:x if obtainMXRecord(queryDomain_) is not None else ''
            # v = lambda x:obtainMXRecord(queryDomain_) or ''
            # print("")
            # print(v)
            # print("check mx record data: " +str(v))
            if obtainMXRecord(queryDomain_):
                queryMXRecord_ = obtainMXRecord(queryDomain_)
                print("MX record seems valid")
                if queryMXRecord_[-1] == ".":
                    # remove dot at the end of string.
                    queryMXRecord_ = queryMXRecord_[:-1]
                    # print(queryMXRecord_)
                else:
                    pass
                try:
                    if checkSMTP(email_address, queryMXRecord_):
                        print("Email found")
                        # return {"message":"Email "+str(email_address)+" is exists", "status": "found"}
                        return jsonify({
                            "message": "Email "+str(email_address)+" is exists",
                            "email": str(email_address),
                            "email_status":"found",
                            "status": "success"
                        }), 200
                    else:
                        print("Email not found")
                        # return {"message": "Email "+str(email_address)+" does not exists", "status": "not found"}
                        return jsonify({
                            "message": "Email "+str(email_address)+" does not exists",
                            "email": str(email_address),
                            "email_status":"not found",
                            "status":"success"
                        }), 200
                # except smtplib.SMTP.timeout as timeout_e:
                #     print(timeout_e)
                #     return "Timeout"
                # except TimeoutError as e:
                #     # print (str(e))
                #     print("badigol")
                except smtplib.socket.timeout as timeout_e:
                    # print(timeout_e)
                    # return str(timeout_e)
                    return jsonify({
                        "message":"Timeout. Unable to access SMTP protocol on domain: "+str(queryDomain_),
                        "status":"failed"
                    }), 408
                except Exception as e:
                    # print(e)
                    template = "[main_program] An exception of type {0} occurred. Arguments:\n{1!r}"
                    message = template.format(type(e).__name__, e.args)
                    print(type(e))
                    # print("e")
                    print(message)
                    print(e)
                    # return "Error found in main_program()"
                    return jsonify({
                        "message": "Error found in main_program(). Please contact Hakase to fix this issue.",
                        "status": "failed"
                    }), 500

            else:
                return jsonify({
                    "message": "Mail Exchange record does not found for this domain: "+str(queryDomain_),
                    "status": "failed"
                }), 404
        else:
            return jsonify({
                "message": "Your email format may broken. Please check if the domain in the email is exists",
                "status": "failed"
            }), 400
    else:
        return jsonify({
            "message": "Email format is not following the standard.",
            "status": "failed"
        }), 400

@app.route("/api/verify", methods=["POST"])
def verifyMail():
    RJSON = request.json
    if RJSON is not None:
        # print(RJSON)
        # print("OK")
        try:
            data = str(RJSON['email'])
            key = str(RJSON['key'])
            # if key == "aezakmi"
            # print(data)
            # print(key)
            # # return "OK"
            # return {"message": "Nice", "status": "success"}
            if key == "aezakmi":
                # print("noice")
                # return "ok"
                return main_program(data)
                # return "OK"
            else:
                pass
                # return "not ok"
                return jsonify({
                    "message":"Key is invalid",
                    "status": "failed"
                }), 401
        # except smtplib.socket.timeout as timeout_error:
        #     print(timeout_error)
        #     return "failed"
        except Exception as e:
            # print("Unauthorized use of API is illegal")
            # return "bruh"
            # print(e.__name__)
            template = "[verifyMail] An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            # print("e")
            print(message)
            # return {"message": "Unauthorized use of API is illegal", "status": "error"}
            return jsonify({
                "message": "Unauthorized use of API is illegal",
                "status": "failed"
            }), 403
    else:
        try:
            return jsonify({
                "message":"Method is not allowed",
                "status": "failed"
            }), 403
        except Exception as e:
            return jsonify({
                "message":"Something is wrong.",
                "status": "failed"
            }), 403

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
