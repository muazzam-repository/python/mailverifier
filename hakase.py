import dns
import dns.resolver as dnsResolve

domain_ = "muazzam.my"
record_ = "mx"
val_dict_ = {}
sum_dict = {}
try:
    r = dnsResolve.resolve(domain_, record_)
    for val in r:
        parse_val = val.to_text().split(" ")
        parsedVal_priority = parse_val[0]
        parsedVal_data = parse_val[1]
        val_dict_[parsedVal_priority] = parsedVal_data
        sum_dict["mx_record"] = "Found"
except dnsResolve.NoAnswer as err_NoA:
    print("Mail Exchange (MX) record does not found for this domain name")
except Exception as e:
    template = "An exception of type {0} occurred. Arguments:\n{1!r}"
    message = template.format(type(e).__name__, e.args)
    # print("e")
    print(message)

if val_dict_:
    print(val_dict_)
    print(sum_dict)
else:
    pass
