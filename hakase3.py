# Email Verifier
# Made with <3 by Muazzam/Hakase
# October 15, 2021

import dns
import dns.resolver as dnsResolve
import re
import smtplib
import sys

email_address = "ma.muazzam0@gmail.com"
# email_address = sys.argv[1]
match = re.match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", email_address)

def verifyFormat(email_address):
    # source: https://stackoverflow.com/a/35094475
    match = re.match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", email_address)
    if match==None:
        # Not in format? return False
        return False
    else:
        # Follow standard email format? return True
        return True
def obtainDomain(email_address):
    try:
        match = email_address.split('@')[1]
        return match
    except IndexError as ee:
        print("Not a valid email address")
        return False
    except Exception as e:
        # print(e.__name__)
        print("Something error: "+str(e))
        return False

def obtainMXRecord(domain_name):
    val_dict_ = {}
    sum_dict_ = {}
    try:
        r = dnsResolve.resolve(queryDomain_, "mx")
        for val in r:
            parse_val = val.to_text().split(" ")
            parsedVal_priority = parse_val[0]
            parsedVal_data = parse_val[1]
            val_dict_[parsedVal_priority] = parsedVal_data
            sum_dict_["mx_record"] = "Found"
    except dnsResolve.NoAnswer as err_NoA:
        print("Mail Exchange (MX) record does not found for this domain name")
    except Exception as e:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)
    getHighestPriority = min(val_dict_, key=int)
    getHighestPriority = val_dict_[getHighestPriority]
    return getHighestPriority

def checkSMTP(email_address, mx_record):
    print("Now we try to connect to this SMTP")
    server = smtplib.SMTP(mx_record)
    server.connect(mx_record, 25)
    server.ehlo()
    server.mail("verifier@gmail.com")
    code, message = server.rcpt(str(email_address))
    # print(server.verify(email_address))
    if code == 250:
        # print("success. email found")
        return True
    else:
        # print("email not found")
        return False

def bannerHakase():
    print('\033c')
    print("Welcome to Hakase Email Verifier")
    print("                     version 0.1")
    print("\n\n")
if __name__ == "__main__":
    bannerHakase()
    try:
        # print(verifyFormat(email_address))
        print("Checking email format...")
        if verifyFormat(email_address):
            print("Email format: Valid")
            # proceed here
            print("Get the domain from email address...")
            if obtainDomain(email_address):
                print("Domain obtained")
                # proceed here
                print("Check MX record for this domain")
                queryDomain_ = obtainDomain(email_address)
                queryMXRecord_ = obtainMXRecord(queryDomain_)
                print("MX record seems valid")
                if queryMXRecord_[-1] == ".":
                    # remove dot at the end of string.
                    queryMXRecord_ = queryMXRecord_[:-1]
                    # print(queryMXRecord_)
                else:
                    pass
                try:
                    if checkSMTP(email_address, queryMXRecord_):
                        print("Email found")
                    else:
                        print("Email not found")
                    # now we try connect to SMTP.
                    # print("Now we try to connect to this SMTP")
                    # server = smtplib.SMTP(queryMXRecord_)
                    # server.connect(queryMXRecord_, 25)
                    # server.ehlo()
                    # server.mail("verifier@gmail.com")
                    # code, message = server.rcpt(str(email_address))
                    # # print(server.verify(email_address))
                    # if code == 250:
                    #     print("success. email found")
                    # else:
                    #     print("email not found")
                except Exception as e:
                    print(e)                
            else:
                print("not ok")
        else:
            print("no ok")
    except Exception as e:
        print("Return E: "+str(e))
