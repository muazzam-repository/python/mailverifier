# the point of this script is for tunneling the `APIHakase.py` service from local to the cloud.
# since many of the cloud services block SMTP port, which is por `25`, we need to find alternative.
# So I decided to host this `APIHakase.py` on my local server, and then reverse tunnel it to the cloud at DigitalOcean.
# 
#
# -N: quite mode. no need tty
# -g: ?
# -R: Reverse tunnel
# 0.0.0.0: bind all IP at the cloud
# 8083: bind port `8083` at the cloud. you can change this later
# localhost: since we host this code at this machine, we just use `localhost`.
# 8080: this is our `APIHakase.py` Flask application. So we want to point it to this service.
# all-for-one-2-reka: this is based on `~/.ssh/config` that we set. Made the job easier.

#
# The other part is we need to uncomment `GatewayPorts no` at the cloud, in this case is DigitalOcean.
# First we need to ssh first to the cloud. Modify `/etc/ssh/sshd_config`, and look for `#GatewayPorts no`.
# Uncomment it, and change it to `GatewayPorts yes`. Save after you modified it.
# Now after modified the sshd config, we want to restart ssh to make sure the modified configuration is reflected.
# Run following: `sudo systemctl restart ssh`
# Now you should be able to run this script.

#!/bin/bash
ssh -N -g -R 0.0.0.0:8083:localhost:8080 all-for-one-2-reka
